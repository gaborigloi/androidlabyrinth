package uk.ac.cam.gi225.labyrinth;

import java.util.HashMap;
import java.util.Map;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.content.res.TypedArray;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Paint;
import android.graphics.drawable.Drawable;
import android.os.Bundle;
import android.view.MotionEvent;
import android.view.View;

// TODO remove numbers
// TODO win -> next level
public class Maze extends Activity {

	@Override
	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		MazeView mView = new MazeView(this);
		setContentView(mView);
	}

	public class MazeView extends View {
		private Bitmap maze1;
		private Player h;
		private Paint p;
		private Paint p2;
		private Paint p_destination;
		private Paint pp;
		private float sx;
		private float sy;
		private int tx;
		private int ty;
		private Drawable playerd;
		private static final int T = 20;
		private static final int SOUTH = 0;
		private static final int EAST = 1;
		private static final int WEST = 2;
		private static final int NORTH = 3;
		private static final int VIEW_SIMPLE = 0;
		private static final int VIEW_ISOMETRIC = 1;

		private int viewtype = VIEW_ISOMETRIC;
		private int width;
		private int height;
		private int twidth;
		private int theight;
		private int origin_x;
		private int origin_y;
		private int otx;
		private int oty;
		private int destination_x;
		private int destination_y;
		private boolean win;
		private int sl;
		private int ow, oh;
		private int hh;

		private Map<Object, Drawable> tex = new HashMap<Object, Drawable>();
		private Drawable[] pd = new Drawable[5];

		@Override
		protected void onSizeChanged(int w, int h, int oldw, int oldh) {
			super.onSizeChanged(w, h, oldw, oldh);
			width = w;
			height = h;
			if (viewtype == VIEW_SIMPLE) {
				sx = (float) ((Math.sqrt(T * width / height) - 1) / 2);
				sy = (float) ((Math.sqrt(T * height / width) - 1) / 2);
			} else if (viewtype == VIEW_ISOMETRIC) {
				double sr = 100 / 58;
				float x = (float) ((height * sr + width) + Math.sqrt((height
						* sr + width)
						* (height * sr + width)
						+ 4
						* (width * height * sr * (T - 1))))
						/ (2 * T - 1);
				tx = (int) (width / x * 2);
				ty = (int) (height / x * sr * 2);
				twidth = (int) x;
				Drawable dd;
				dd = getResources().getDrawable(R.drawable.path1);
				ow = dd.getIntrinsicWidth();
				oh = dd.getIntrinsicHeight();
				hh = Math.round((float) twidth / (float) ow * (float) oh);
				theight = hh;
				otx = width / 2 - twidth / 2 - (tx - ty) * twidth / 2;
				oty = height / 2 - theight / 2 - (tx + ty) * theight / 2;
			}
		}

		public MazeView(Context context) {
			super(context);
			Intent intent = getIntent();
			sl = intent.getIntExtra(SelectMaze.SELECTED_MAZE, 0);
			playerd = getResources().getDrawable(R.drawable.pwethen);
			pp = new Paint();
			pp.setARGB(255, 255, 0, 0);
			pp.setTextSize(30.0f);
			h = new Player();
			win = false;
			BitmapFactory.Options opt = new BitmapFactory.Options();
			opt.inScaled = false;
			int[] mazeind = getResources().getIntArray(R.array.mazes);
			maze1 = BitmapFactory.decodeResource(getResources(), mazeind[sl]);
			int[] texcolours = getResources().getIntArray(R.array.colours);
			TypedArray texdrawables = getResources().obtainTypedArray(
					R.array.tex);
			pd[0] = getResources().getDrawable(R.drawable.pwethen);
			pd[1] = getResources().getDrawable(R.drawable.pwethen);
			pd[2] = getResources().getDrawable(R.drawable.pwethen);
			pd[3] = getResources().getDrawable(R.drawable.pwethen);

			p = new Paint();
			p.setARGB(255, 255, 0, 0);
			p.setTextSize(30);
			p2 = new Paint();
			p2.setARGB(255, 0, 255, 140);
			p_destination = new Paint();
			p_destination.setARGB(255, 0, 0, 255);
			for (int i = 0; i < maze1.getWidth(); i++)
				for (int j = 0; j < maze1.getHeight(); j++) {
					if (maze1.getPixel(i, j) == Color.RED) {
						origin_x = i;
						origin_y = j;
					}
					if (maze1.getPixel(i, j) == Color.GREEN) {
						destination_x = i;
						destination_y = j;
					}
					if (!tex.containsKey(maze1.getPixel(i, j))) {
						int k = 0;
						while ((k < 2)
								&& (texcolours[k] != maze1.getPixel(i, j)))
							k++;
						if (k < 2)
							tex.put(maze1.getPixel(i, j),
									texdrawables.getDrawable(k));
					}
				}

			h.Set(origin_x, origin_y);
			Drawable dd = getResources().getDrawable(R.drawable.path1);
			theight = Math.round((float) twidth
					/ (float) (dd.getIntrinsicWidth())
					* (float) (dd.getIntrinsicHeight()));
		}

		@Override
		protected void onDraw(Canvas canvas) {
			if (viewtype == VIEW_SIMPLE) {
				for (int i = (int) (h.x - sx); i < h.x + sx; i++)
					for (int j = (int) (h.y - sy); j < h.y + sy; j++)
						if ((i >= 0) && (j >= 0) && (i < maze1.getWidth())
								&& (j < maze1.getHeight())) {
							float dx = i - h.x + sx;
							float dy = j - h.y + sy;
							float rwidth = width / (2 * sx + 1);
							float rheight = height / (2 * sy + 1);
							if (maze1.getPixel(i, j) == Color.BLACK)
								canvas.drawRect(dx * rwidth, dy * rheight,
										(dx + 1) * rwidth, (dy + 1) * rheight,
										p);
							else if (maze1.getPixel(i, j) == Color.GREEN)
								canvas.drawRect(dx * rwidth, dy * rheight,
										(dx + 1) * rwidth, (dy + 1) * rheight,
										p_destination);
						}
				canvas.drawText(Integer.toString(maze1.getHeight()), 20, 20, p);
				canvas.drawText(Integer.toString(maze1.getWidth()), 20, 60, p);
				canvas.drawText(Integer.toString(maze1.getPixel(1, 1)), 40,
						140, p2);
				canvas.drawRect(sx * width / (2 * sx + 1), sy * height
						/ (2 * sy + 1), (sx + 1) * width / (2 * sx + 1),
						(sy + 1) * height / (2 * sy + 1), p2);
			} else if (viewtype == VIEW_ISOMETRIC) {
				for (int i = h.x - tx; i <= h.x + tx; i++)
					for (int j = h.y - ty; j <= h.y + ty; j++)
						if ((i >= 0) && (j >= 0) && (i < maze1.getWidth())
								&& (j < maze1.getHeight())) {
							{
								int dx = (i - (h.x - tx));
								int dy = (j - (h.y - ty));
								int tx = otx + dx * twidth / 2 - dy * twidth
										/ 2;
								int ty = oty + dy * theight / 2 + dx * theight
										/ 2;
								if (tex.containsKey(maze1.getPixel(i, j))) {
									Drawable c = tex.get(maze1.getPixel(i, j));
									c.setBounds(
											tx,
											ty
													+ theight
													- Math.round((float) twidth
															/ (float) (c
																	.getIntrinsicWidth())
															* (float) (c
																	.getIntrinsicHeight())),
											tx + twidth, ty + theight);
									c.draw(canvas);
								}
								if ((i == h.x) && (j == h.y)) {
									// playerd.setBounds(tx,ty + theight -
									// Math.round((float) twidth/(float)
									// (playerd.getIntrinsicWidth()) * (float)
									// (playerd.getIntrinsicHeight())),tx+twidth,
									// ty+ theight);
									// playerd.draw(canvas);
									pd[h.dir]
											.setBounds(
													tx,
													ty
															+ theight
															- Math.round((float) twidth
																	/ (float) (playerd
																			.getIntrinsicWidth())
																	* (float) (playerd
																			.getIntrinsicHeight())),
													tx + twidth, ty + theight);
									pd[h.dir].draw(canvas);
								}
								// canvas.drawBitmap(pb, width/2-twidth/2,
								// height/2-theight/2+(theight-pb.getHeight()),
								// null);

							}

						}
				canvas.drawText(Integer.toString(hh), 40, 40, pp);
				canvas.drawText(
						Integer.toString(theight) + " " + Integer.toString(sl),
						40, 80, pp);
			}
		}

		public boolean onTouchEvent(MotionEvent event) {
			if (!win) {
				if ((event.getY() < height / 3)
						&& (event.getX() < width / 3 * 2)
						&& (event.getX() > width / 3) && (h.y > 0))
					h.Move(NORTH);
				else if ((event.getY() < height / 3 * 2)
						&& (event.getY() > height / 3)
						&& (event.getX() > width / 3 * 2)
						&& (h.x < maze1.getWidth() - 1))
					h.Move(EAST);
				else if ((event.getY() < height / 3 * 2)
						&& (event.getY() > height / 3)
						&& (event.getX() < width / 3) && (h.x > 0))
					h.Move(WEST);
				else if ((event.getY() > height / 3 * 2)
						&& (event.getX() > width / 3)
						&& (event.getX() < width / 3 * 2)
						&& (h.y < maze1.getHeight() - 1))
					h.Move(SOUTH);
			}
			return false;
		}

		public class Player {
			public int x, y, dir;

			public void Set(int x, int y) {
				this.x = x;
				this.y = y;
			}

			void Move(int dir) {
				this.dir = dir;
				switch (dir) {
				case EAST:
					if (maze1.getPixel(x + 1, y) != Color.BLACK)
						x++;
					break;
				case SOUTH:
					if (maze1.getPixel(x, y + 1) != Color.BLACK)
						y++;
					break;
				case NORTH:
					if (maze1.getPixel(x, y - 1) != Color.BLACK)
						y--;
					break;
				case WEST:
					if (maze1.getPixel(x - 1, y) != Color.BLACK)
						x--;
					break;
				default:
				}
				if ((x == destination_x) && (y == destination_y))
					win = true;
				invalidate();
			}

		}
	}

}
