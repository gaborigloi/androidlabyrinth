package uk.ac.cam.gi225.labyrinth;

import android.app.ListActivity;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.ArrayAdapter;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

//TODO fix 2nd labyrinth
public class SelectMaze extends ListActivity {
	
	public final static String SELECTED_MAZE = "com.asdf.android.labyrinth.MAZE";
	
	@Override
	public void onCreate(Bundle savedInstanceState) {
	  super.onCreate(savedInstanceState);
	  
	  String[] MAZES = getResources().getStringArray(R.array.mazes_array);
	  setListAdapter(new ArrayAdapter<String>(this, R.layout.list_item, MAZES));

	  ListView lv = getListView();
	  lv.setTextFilterEnabled(true);
	  

	  lv.setOnItemClickListener(new OnItemClickListener() {
	    public void onItemClick(AdapterView<?> parent, View view,
	        int position, long id) {
	      // When clicked, show a toast with the TextView text
	      Toast.makeText(getApplicationContext(), /*((TextView) view).getText()*/Integer.toString(position),
	          Toast.LENGTH_SHORT).show();
	      Intent intent = new Intent(SelectMaze.this,Maze.class);
	      intent.putExtra(SELECTED_MAZE, position);
			startActivity(intent);
	    }
	  });
	}

}
