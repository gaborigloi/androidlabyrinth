package uk.ac.cam.gi225.labyrinth;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;

// TODO icon
public class LabyrinthActivity extends Activity implements OnClickListener {

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.main);
        Button button = (Button)findViewById(R.id.button_exit);
        button.setOnClickListener(this);
        button = (Button)findViewById(R.id.button_start);
        button.setOnClickListener(this);
        
    }

	public void onClick(View v) {
		if (v.getId()==R.id.button_exit)
			finish();
		else if (v.getId()==R.id.button_start) {
			Intent intent = new Intent(this,SelectMaze.class);
			startActivity(intent);
		}
		
	}
}